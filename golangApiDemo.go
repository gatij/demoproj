package main

import (
    "context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	ID        	primitive.ObjectID 		`json:"id,omitempty" bson:"_id,omitempty"`
	Firstname 	string             		`json:"firstname,omitempty" bson:"firstname,omitempty"`
	Lastname  	string             		`json:"lastname,omitempty" bson:"lastname,omitempty"`
	Email	  	string  			 	`json:"email,omitempty" bson:"email,omitempty"`
}

type Employee struct {
	ID        	primitive.ObjectID   	`json:"id,omitempty" bson:"_id,omitempty"`
	Designation string 			   		`json:"designation,omitempty" bson:"designation,omitempty"`
	UserID    	primitive.ObjectID 		`json:"userid,omitempty" bson:"_userid,omitempty"`
}

type CreateUserApiRequest struct{
	Firstname 	string             		`json:"firstname,omitempty" bson:"firstname,omitempty"`
	Lastname  	string             		`json:"lastname,omitempty" bson:"lastname,omitempty"`
	Email	  	string  			 	`json:"email,omitempty" bson:"email,omitempty"`
	Designation string 			   		`json:"designation,omitempty" bson:"designation,omitempty"`
}

type FetchUserApiRequest struct{
	ID 			primitive.ObjectID   `json:"id,omitempty" bson:"_id,omitempty"`
}

type FetchUserApiResponse struct{
	Firstname 	string             		`json:"firstname,omitempty" bson:"firstname,omitempty"`
	Lastname  	string             		`json:"lastname,omitempty" bson:"lastname,omitempty"`
	Email	  	string  			 	`json:"email,omitempty" bson:"email,omitempty"`
	Designation string 			   		`json:"designation,omitempty" bson:"designation,omitempty"`
	EmpID       primitive.ObjectID      `json:"empid,omitempty" bson:"empid,omitempty"`
}

type UpdateUserApiRequest struct{
	ID 			primitive.ObjectID   `json:"id,omitempty" bson:"_id,omitempty"`
	Email       string 	             `json:"email,omitempty" bson:"email,omitempty"`
}


var client *mongo.Client


func main() {

    clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
    client, err := mongo.Connect(context.TODO(), clientOptions)

    if err != nil {
        log.Fatal(err)
    }

    err = client.Ping(context.TODO(), nil)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("Connected to MongoDB!")

    router := mux.NewRouter()

    router.HandleFunc("/assignment/user", CreateUserEndpoint).Methods("POST")
	router.HandleFunc("/assignment/user", GetUserEndpoint).Methods("GET")
	router.HandleFunc("/assignment/user", UpdateUserEndpoint).Methods("PATCH")
    
    log.Fatal(http.ListenAndServe(":12345", router))

    defer client.Disconnect(context.TODO())

}  



func CreateUserEndpoint(response http.ResponseWriter, request *http.Request) {
	
	response.Header().Set("content-type", "application/json")

	var createUserData CreateUserApiRequest
	_ = json.NewDecoder(request.Body).Decode(&createUserData)

	var user User
	user.Firstname = createUserData.Firstname
	user.Lastname = createUserData.Lastname
	user.Email = createUserData.Email

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	userCollection := client.Database("mygodb").Collection("UserCollection")
	employeeCollection := client.Database("mygodb").Collection("EmployeeCollection")
		
	insertResult, err := userCollection.InsertOne(context.TODO(), user)
	if err != nil {
        log.Fatal(err)
    }

    fmt.Println("Inserted a Single Document in userCollection: ", insertResult.InsertedID)
	
	var emp Employee
	emp.Designation = createUserData.Designation
	emp.UserID = insertResult.InsertedID.(primitive.ObjectID)


	empInsertResult, er := employeeCollection.InsertOne(context.TODO(),emp)
	if er != nil {
        log.Fatal(er)
    }

	fmt.Println("Inserted a Single Document in employeeCollection: ", empInsertResult.InsertedID)

	defer client.Disconnect(context.TODO())

	json.NewEncoder(response).Encode(insertResult)
} 


func GetUserEndpoint(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")

	
	var userid FetchUserApiRequest
	_ = json.NewDecoder(request.Body).Decode(&userid)

	fmt.Println("from req body: ", userid.ID)
	
	var user User
	var emp Employee

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ := mongo.Connect(context.TODO(), clientOptions)
	userCollection := client.Database("mygodb").Collection("UserCollection")
	employeeCollection := client.Database("mygodb").Collection("EmployeeCollection")

	err := userCollection.FindOne(context.TODO(), User{ID: userid.ID}).Decode(&user)

	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}


	er := employeeCollection.FindOne(context.TODO(), Employee{UserID: userid.ID}).Decode(&emp)

	if er != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + er.Error() + `" }`))
		return
	}


	var fetchUserData FetchUserApiResponse
	fetchUserData.Firstname = user.Firstname
	fetchUserData.Lastname = user.Lastname
	fetchUserData.Email = user.Email
	fetchUserData.Designation = emp.Designation
	fetchUserData.EmpID = emp.ID

	defer client.Disconnect(context.TODO())

	json.NewEncoder(response).Encode(fetchUserData)
	
}

func UpdateUserEndpoint(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")

	var userUpdateData UpdateUserApiRequest
	_ = json.NewDecoder(request.Body).Decode(&userUpdateData)

	fmt.Println("from req body ID is: ", userUpdateData.ID)
	fmt.Println("from req body Email is: ", userUpdateData.Email)

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ := mongo.Connect(context.TODO(), clientOptions)
	userCollection := client.Database("mygodb").Collection("UserCollection")

	result, err := userCollection.UpdateOne(
    context.TODO(),
    bson.M{"_id": userUpdateData.ID},
    bson.D{
        {"$set", bson.D{{"email", userUpdateData.Email}}},
    },
)

	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	
	fmt.Println("email updated!")

	defer client.Disconnect(context.TODO())

	json.NewEncoder(response).Encode(result)
}
